FROM arkye/elixir:alpine AS build

RUN apk add git

COPY .git .git
COPY lib lib
COPY mix.exs mix.lock ./

RUN mix deps.get && \
  mix deps.compile && \
  mix escript.build

FROM arkye/docker-compose

RUN apk add --no-cache elixir

COPY --from=build /app/rit /usr/local/bin/rit
